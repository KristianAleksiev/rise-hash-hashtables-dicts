﻿namespace DictionaryOfWords
{
    public class DictionaryOfWords
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the correct word dictionary");
            string[] correctDict = Console.ReadLine().Split(" ");

            Console.WriteLine("Please enter a string for a word-by-word type check:");            
            string[] inputWords = Console.ReadLine().Split(" ");            
            
            HashSet<string> misspelledWords = GetMispelledfWords(correctDict, inputWords);

            if (misspelledWords != null) {
                foreach (string word in misspelledWords)
                {
                    Console.WriteLine($"The word '{word}' is not written correctly!");
                }
            } 
        }

        public static HashSet<string> GetMispelledfWords(string[] inputDict, string[] checkedWords)
        { 

            HashSet<string> mispelledWords = new HashSet<string>(); 

            foreach (string word in checkedWords) {
                if (!inputDict.Contains(word))
                {
                    mispelledWords.Add(word);
                }
            }
            
            return mispelledWords;
        }
    }
}