﻿using System.Security.Cryptography.X509Certificates;

namespace NonRepeatingCharacters
{
    public class NonRepeatingCharacters
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a string");
            string input = Console.ReadLine();

            Dictionary<string, int> nonRepeating = new Dictionary<string, int>();

            bool noneNonRepeating = false;


            foreach (char current in input)
            {
                if (!nonRepeating.ContainsKey(current.ToString()))
                {
                    nonRepeating[current.ToString()] = 0;
                }
                nonRepeating[current.ToString()] += 1;
            }


            foreach (KeyValuePair<string, int> pair in nonRepeating)
            {
                if (pair.Value != 1) {
                    nonRepeating.Remove(pair.Key);
                    continue;
                }
                Console.WriteLine($"Non-repeating character -> {pair.Key}");
            }

            var result = getNoneNonRepeatingOrIndex(nonRepeating, input);
            Console.WriteLine($"The index of the first element which does not reapeat is: {result}");
        }
        static int getNoneNonRepeatingOrIndex(Dictionary<string, int> nonRepeatingDict, string input) { 
            if (nonRepeatingDict.Count == 0)
            {
                return 1;
            }
            return input.IndexOf(nonRepeatingDict.First().Key);

        }
    }
}