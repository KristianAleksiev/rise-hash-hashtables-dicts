﻿using System.Collections;

namespace HashTable{
    public class HashTable
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> kat = new Dictionary<string, string>();

            kat.Add("Kristian", "E1149MT");
            kat.Add("Konstantin", "CB2390TP");
            kat.Add("Petyr", "CT1919KM");
            kat.Add("Georgi", "CB2222MP");
            kat.Add("Simona", "PB2021ET");
            kat.Add("Ekaterina", "CA2025HH");

            foreach (KeyValuePair<string, string> log in kat)
            {
                Console.WriteLine($"Current owner: {log.Key}, License plate: {log.Value}");

            }
            Console.WriteLine("=================================================================");

            kat.Remove("Kristian");
            kat.Remove("Georgi");

            foreach (KeyValuePair<string, string> log in kat) 
            { 
                Console.WriteLine($"Current owner: {log.Key}, License plate: {log.Value}");
              
            }


            Console.WriteLine("=================================================================");
            Console.WriteLine("=================================================================");

            Hashtable ageRegister = new();
            
            ageRegister.Add("Kristian", 25);
            ageRegister["Kristian"] = 26;
            Console.WriteLine(ageRegister["Kristian"]);
            ageRegister.Add("Dimitrina", "Dvadeset i dve");

            foreach (DictionaryEntry current in ageRegister)
            {
                Console.WriteLine($"{current.Key}, {current.Value}");

            }
        }        
    }
}